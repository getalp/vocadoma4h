# The VocADom@A4H corpus

This directory contains a corpus of about 12 hours of smart home data from 11 different recording sessions in the <a href="https://amiqual4home.inria.fr">Amiqual4Home smart home</a>. The experiment was conducted between May and June 2017 as part of the <a href="http://vocadom.imag.fr">VocADom project</a> supported by the <a href="https://anr.fr">Agence Nationale de la Recherche</a> under grant ANR-16-CE33-0006. 

If you use the corpus or need more details please refer to the following paper: *Context-Aware Voice-based Interaction in Smart Home -VocADom@A4H Corpus Collection and Empirical Assessment of its Usefulness*

```
@InProceedings{portet:hal-02165532,
  author = 	"Portet, Fran{\c c}ois and Caffiau, Sybille and Ringeval, Fabien and Vacher, Michel and Bonnefond, Nicolas and Rossato, Solange and Lecouteux, Benjamin and Desot, Thierry",
  title = 	"Context-Aware Voice-based Interaction in Smart Home - VocADom@A4H Corpus Collection and Empirical Assessment of its Usefulness",
  booktitle = 	"17th IEEE International Conference on Pervasive Intelligence and Computing (PICom 2019)",
  year = 	"2019",
  location = 	"Fukuoka, Japan",
  url = 	"https://hal.archives-ouvertes.fr/hal-02165532"
}
```
## Aims and protocol of the recording

The experiment was performed to study voice commands in multi-room smart home and in a multi-dweller setting. Usual home automation sensors (movement detector, contact door detector, temperature, etc.) as well as arrays of microphone signals were captured. 
Eleven participants uttered voice commands while performing scripted activities of daily living for about one hour of recording per participant. At the beginning of each session for each participant, the voice command grammar was not imposed as the aim was to elicit spontaneous speech. Then, the participants had to follow an increasingly constrained grammar. Using a Wizard-of-Oz approach, out-of-sight experimenters enacted user commands, acting as a ‘perfect’ voice command system.

For each participant, the whole experiment session was recorded continuously without interruption. Within a session, 3 phases were identified:
 
    Phase 1 – Graphical based instruction to elicit spontaneous voice commands (interaction with the home)

    Phase 2 – Inhabitant scenario enacting a visit by a friend (interaction with the home and the visitor)

    Phase 3 – Voice commands in noisy domestic environment (reading of voice commands in the home - no interaction)

This data set is intended to be useful for the following (not exclusive) tasks :

- multi-Human localization
- multi-Human activity recognition
- smart home context modeling
- multi-channel Voice activity detection 
- multi-channel Automatic Speech Recognition
- multi-channel Spoken Language Understanding
- multi-channel Speaker Recognition
- multi-channel speech enhancement
- multi-channel blind source separation
- multi-channel automatic decision making


## Organization of the repository

The repository is composed of:
* the *doc/* directory which contains information about the sensors used, the smart home layout and the sensors position as well as participants' demographics. Feel free to suggest other information to include when needed
* the *record/* directory which contains the data of the eleven recording sessions.
* the *Term_of_Use* file specify what can be done with the data set under the GDPR regulation


## Details of the corpus
* All the data is stored under the *record/* directory which contains 11 sub-directories named S[00-10]/. Each of these respects the following structure : 
    - openhab_log/		(log of the home automation network) 			publicly available

    - activity/ 		(annotation of the participants' location and activity) publicly available

    - mic_array/ 		(microphone array recordings) 				available upon request

    - mic_headset/ 	(headset microphone recordings) 			available upon request

    - speech_transcript/ 	(transcription of the participants' speech) 		available upon request

    - NLU/ 		(semantic annotation of the voice commands) 		available upon request

    - phases.csv		(time period of each phase of the recording)		publicly available

	

### openhab logs

- S??_change.csv.log: the timestamped log of sensor values changes (e.g., door moving from CLOSED to ON). Each log covers more that the experiment duration to provide a view on the initial value of the sensors.
- S??_wizard.csv.log: the timestamped log of commands sent through the openHAB network by the Wizards.

The list of sensors and their location in the flat can be found in *doc/smarthome/* directory. Each line, follows the structure below 
<pre>
[A4HID_(when exists)]room_(object|function|object_function)[_locationDetail]	domain value 	[unit]
</pre>

the Amiqual4Home ID (A4HID) is used to indicate its position in the floor plan. For instance, the line 
<pre>
 global_coldwater_instantaneous	Number	l/s
</pre>
means that the measure of the instantaneous *cold water* of the entire house (*global*) is a real *Number* using the liter per second (*l/s*) unit <br>

while the following line 
<pre>
L1_bedroom_downlight_drawers_side	{0, 100}
</pre>
indicates that the *downlight* lamp identified by the *L1* ID is situated in the *bedroom* close of the chest of *drawers*. The domain value is in the range of [0,100] which represents a ratio (in percent) of lighting strength.
 

### activity
- csv file organized as :
	interval start ; interval end ; location - activity dweller 1 ;  location - activity dweller 2 ;  location - activity dweller 3 ;...

- location is within {bathroom, bedroom, corridor, stairs, entrance, kitchen, livingroom, office, out, toilet} 
- activity is a description free text (e.g., sitting on the sofa and reading, talking and setting up the audio, etc.)

### mic_array

- The 16-channel recording of the experiment was performed by 4 arrays of 4 microphones arranged in a square of 10cm side
- each microphone was a t.bone LC 97 TWS (https://www.thomann.de/fr/the_tbone_lc97_tws.htm)
- recording was performed using Kristal Audio Engine : Version 1.0.1 (Jun 1 2004) on a Windows 8.1 64
- each channel is a mono 32-bit Floating Point PCM acquired at 44.1kHz
- array I (resp II, III, IV) is composed of channel_[1-4].wav (resp [5-8], [9-12], [13-16]) 
- cf. the floor plan for precise location of these arrays

### mic_headset
- wireless microphone worn by the participant
- it was a SENNHEISER HSP4 -ew-3 static cardioide, jack 3.5
- the recording was performed using Audacity : 2.0.5 on a Ubuntu 14.04 LTS 64
- mono 16-bit Signed Integer PCM acquired at 16kHz

### speech_transcript
- participant transcription of the headset speech 
- in transcriber format (http://trans.sourceforge.net/en/)

### NLU
- semantically labeled transcription
  See the following paper for details

```
@InProceedings{desot:hal-01802758,
  author = 	"Desot, Thierry and Raimondo, Stefania and Mishakova, Anastasia. and Portet, Fran{\c c}ois and Vacher, Michel",
  title = 	"Towards a French Smart-Home Voice Command Corpus: Design and NLU Experiments",
  booktitle = 	"21st International Conference on Text, Speech and Dialogue TSD 2018",
  year = 	"2018",
  publisher = 	"Association for Computational Linguistics",
  pages = 	"509--517",
  location = 	"Brno, Czech Republic",
  url = 	"https://hal.archives-ouvertes.fr/hal-01802758"
}
```

### phases.csv 
- file indicating the absolute time of the recording and the phases 
- *ALL* the other files have been synchronized with respect to the absolute start of the recording
- The file respects the following format 
<pre>
	0;date;date # whole recording absolute start and end time
	1;date;date # absolute start and end time of phase 1 
	2;date;date # absolute start and end time of phase 2
	3;date;date # absolute start and end time of phase 3
</pre>

### Synchronization and issues with the corpus

Four computers were used to record respectively the video, the array of microphones, the worn microphone and the openHAB logs. Unfortunately, the different computers used for the recording were not perfectly synchronized. To permit post synchronization, a clapperboard technique was used. For each recording, one of the cupboard doors in the kitchen was opened and closed firmly at least three times in a row. This was captured on the openHAB logs (door sensor), the array of microphone (we used channel 4), the worn sensor and the video camera of the kitchen.

ALL the files have been synchronized with respect to these clapperboard occurrences. 

*Known Issues:*
1. 15 first minutes of the worn microphone of S04 are missing. They have been padded with silence. Padding was added using the sox pad option
2. for S05 and S06 the array of microphones recording has been damaged. These files have not been recovered and hence cannot be used with confidence.


## ARTWORK 
- Some artwork can be heard on the acoustic signal. They are copyrighted under a creative common license. Below are the creative common artwork that were used during the experiment without any modification of their content:
    - Title: *Stargazing*<br>
           Author: Assets<br>
           (CC BY-NC-SA)     <br>     
           Downloaded: May 2017<br>
           Wizard command: music_playfile;radio_inter<br>
    - Title: *Creative commons: un contrat libre pour copier Arte Radio*<br>
	   Author: Matthieu Crocq<br>
           Production: ARTE Radio.com<br>
           (CC BY-NC-ND-2.0)        <br>
           Downloaded: May 2017<br>
           Wizard command: music_playfile;radio_cheriefm<br>
    - Title: *Le dernier souffle* <br>
	   Author: Floriant Debu<br>
	   Production: SYNAPS Collectif Audiovisuel<br>
           (CC BY-NC) <br>
           Downloaded: May 2017<br>
           Wizard command: tv_playMCFile;fr2souffle.mp4          <br>
    - Title: *MOUTONS 2.0 – La puce à l'oreille*<br>
	   Author: Antoine Costa et Florian Pourchi<br>
	   Production: SYNAPS Collectif Audiovisuel<br>
           (CC BY-NC-SA)<br>
           Downloaded: May 2017<br>
           Wizard command: tv_playMCFile;tf1mouton.mp4<br>

- Also feedback to user in case of closed questions (affirmative/negative) or misunderstanding (incomprehensible order/missing keyword) was provided through four types of sound downloaded from https://lasonotheque.org/ and http://www.universal-soundbank.com websites which are banks of short sounds entirely free of copyright. In particular, below are the files and the commands used by the wizard to play them:
	- lasonotheque.org: Sound number: 0758 (Beeps of digital code)<br>
          Downloaded: May 2017<br>
          Wizard command: music_playfile;not_understood<br>
	- lasonotheque.org: Sound number: 0342 (Censor beep 1)<br>
          Downloaded: May 2017<br>
          Wizard command: music_playfile;no_keyword<br>
	- universal-soundbank.com: Sound number: 297 (Fairy bell)<br>
          Downloaded: May 2017<br>
	  Wizard command: music_playfile;affirmative<br>
	- universal-soundbank.com: Sound number: 4067 (End game)<br>
          Downloaded: May 2017<br>
	  Wizard command: music_playfile;negative<br>

### Semi-automatic annotation 
- The guidelines manual_annotation_elan in folder automatic_annotation explains the semi-automatic annotation of the corpus.
 

